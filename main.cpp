﻿// Interpolation.cpp: определяет точку входа для приложения.
//

#include "Lagrange.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>



using namespace std;
//measuring step parameter
enum STATUS 
{
	 
	OK=0,
	FULL,
	EMPTY

};
namespace Interpolation 
{
	double max(double value1, double value2, double value3)
	{
		double max;

		if (value1 > value2)
		{
			max = value1;
		}
		else
		{
			max = value2;
		}
		if (max < value3)
		{
			max = value3;
		}
		return max;
	}

	double lagrange(double lx[], double ly[], double  t) {
		double sum, prod;
		sum = 0;
		
		for (int j = 0; j < 8; j++)
		{
			prod = 1;
			for (int i = 0; i < 8; i++)
			{
				if (i != j)
				{
					prod = prod * (t - lx[i]) / (lx[j] - lx[i]);
				}
			}
			sum = sum + ly[j] * prod;
		}
		return sum;
	}
	//interpolation 
	double interpolation(double x, int N, double lx[], double ly[])
	{
		double sum = 0.0;
		for (int i = 0; i < N; i++)
		{
			double l = 1;
			for (int j = 0; j < N; j++)
			{
				if (j != i)
				{
					l *= (x - lx[j]) / (lx[i] - lx[j]);
				}
			}
			sum += ly[i] * l;
		}		
		return sum;
	}

	/*
	 * Двигаем от нулевого элемента в сторону первого элемента.
	 * Первый элемент затирается. На последний элемент становится newY.
	 */
	int32_t AddNewY(double ly[], int32_t counter, double newY)
	{
		for (int32_t i = 0; i < counter - 1; i++)
		{
			ly[i] = ly[i + 1];
		}
		ly[counter - 1] = newY;
		return 0;
	}

}

#define _USE_MATH_DEFINES
#include <math.h>

int main()
{
	const int32_t N = 9;
	double lx[N];
	double ly[N];
	const double F1 = 8, F2 = 2, F3 = 3;
	const double STEP = 1.0 / (10 * Interpolation::max(F1, F2, F3));
	const double increment = 2 * M_PI * STEP;
	double counter = 0.0;
	std::cout << "max = " << STEP << std::endl;

	std::cout << "lx() = \n";
	for (int i= 0; i < N; i++)
	{
		lx[i] = i * STEP;
		std::cout << lx[i] << endl;
	}
	std::cout << endl << endl; 

	std::cout << "ly()" << endl;
	for (int i = 0; i < N; i++)
	{
		ly[i] = sin(counter * F1 ) + sin(counter * F2) + sin(counter * F3);
		counter += increment;
		std::cout << ly[i] << std::endl;
	}
	std::cout << endl << endl;


	const double zero = lx[N / 2];
	std::cout << "zero = " << zero << endl << endl;
	cout << "point = " << zero + STEP * 0.25 << endl << endl;
	for (int32_t i = 0; i < 4; i++)
	{
		double interpY = Interpolation::interpolation(zero + STEP * 0.25, N, lx, ly);
		std::cout << interpY << std::endl << std::endl;
		double newY = sin(counter * F1) + sin(counter * F2) + sin(counter * F3);
		counter += increment;
		Interpolation::AddNewY(ly, N, newY);
		for (int32_t j = 0; j < N; j++)
		{
			std::cout << ly[j] << std::endl;
		}
		std::cout << std::endl;
	}

	return 0;
}

