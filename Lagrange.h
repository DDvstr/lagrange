#ifndef LAGRANGE_H
#define LAGRANGE_H
#include <cstdint>

class Lagrange
{

public: 
	Lagrange();
	~Lagrange();

	int32_t Init(uint32_t numberOfNodes = 9);
	int32_t Deinit();

	double GetNewY(double dx);

	int32_t UpdateNodes(double Next_Y);
	void Shift(); 
	int32_t Lengthmass; 

	int32_t* mass;
	
	int32_t CounterValues;
	
private:
	int32_t NumberOfNodes;
	double* Dx;
	double* Nodes;

};


#endif 